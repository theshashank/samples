package com.prosoft.sample.service;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

public class DateUtil {

	public static Date getPreviousDate(){
		return DateUtils.addDays(new Date(), -1);
	}
	
	public static Date getNextDate(){
		return DateUtils.addDays(new Date(), 1);
	}
	
}

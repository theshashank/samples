package com.prosoft.sample;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.prosoft.sample.service.DateUtil;

public class MyServlet extends HttpServlet {

	private static final long serialVersionUID = -1859850727402184473L;
	
	public void init() throws ServletException {
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/view.jsp");
		request.setAttribute("currentDate", new Date());
		request.setAttribute("nextDate", DateUtil.getNextDate());
		request.setAttribute("previousDate", DateUtil.getPreviousDate());
		dispatcher.forward(request, response);
	}

	public void destroy() {
	}
}

<html>
<head>
<title>
	Sample Operations
</title>
</head>
<body>
<table>
<tr>
<td>Current Date</td>
<td><%= request.getAttribute("currentDate")%></td>
</tr>

<tr>
<td>Next Date</td>
<td><%= request.getAttribute("nextDate")%></td>
</tr>

<tr>
<td>Previous Date</td>
<td><%= request.getAttribute("previousDate")%></td>
</tr>
</table>
<br/>
<a href="<%= request.getContextPath()%>/index.jsp">Go back</a>
</body>
</html>